/*
 * LSM9DSO.h
 *
 *  Created on: Apr 13, 2022
 *      Author: Admin
 */
//#include "stm32f1xx_hal.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <registers_LSM9DS0.h>
#include "main.h"
#include <stdint.h>
#include <stdlib.h>

#ifndef INC_LSM9DSO_H_
#define INC_LSM9DSO_H_

typedef enum {
    DEVICE_ERROR = 0,
    DEVICE_NORMAL,
    DEVICE_ENABLE,
    DEVICE_DISABLE
} _adcs_sensor_status;



extern SPI_HandleTypeDef spi_sel;

typedef struct {
	int16_t gyr_prev_raw[3];
	float gyr_prev_cal[3];
	int16_t gyr_raw[3];
	float gyr_cal[3];
//	_adcs_sensor_status gyr_status;
	int max_value_g_index; // 245 dps (default value from CTRL_REG4_G reg, FS1-FS0 bits = 0)
	float calib_arr_gyro[3];
	float gyr_base_sensitivity;
	float gyr_percentage_temp_sens_change;
	float current_sens_gyr;
	float current_offset_gyro;

	//arr[0] -> x axis, arr[1] -> y axis, arr[2] -> z axis
	int16_t acc_prev_raw[3];
	float acc_prev_cal[3];
	int16_t acc_raw[3];
	float acc_cal[3];
	_adcs_sensor_status acc_status;
	int max_value_acc_index; // 2 g (default value from CTRL_REG2_XM reg, AFS[2:0] bits = 0)
	float calib_arr_acc[5];
	float acc_base_sensitivity;
	float acc_percentage_temp_sens_change;
	float current_sens_acc;
	float current_offset_acc;

	int16_t mag_prev_raw[3];
	float mag_prev_cal[3];
	int16_t mag_raw[3];
	float mag_cal[3];
	_adcs_sensor_status mag_status;
	int max_value_mag_index; // 2 gauss (default value from CTRL_REG6_XM reg, MFS1-MFS0 bits = 0)
	float calib_arr_mag[4];
	float mag_base_sensitivity;
	float mag_percentage_temp_sens_change;
	float current_sens_mag;
	float current_offset_mag;

	int temp_prev_raw;
	float temp_prev_cal;
	int temp_raw;
	float temp_cal;

	int16_t temp_mul_factor; //

}_lsm9ds0_sensor;

extern _lsm9ds0_sensor sensor;
extern _lsm9ds0_sensor *imu;
extern uint8_t fifo_stream_g;
extern uint8_t fifo_stream_xm;

void init_LSM9DS0_struct();

void select_spi(SPI_HandleTypeDef spi);
void un_select_LSM9DS0_gyro(uint8_t select); //select = 0 -> select, select = 1 -> unselect
void un_select_LSM9DS0_acc_mag(uint8_t select);

void LSM9DS0_init_gyro();
void LSM9DS0_init_acc_mag();

//@param types:
	//R+NOT_MUL_RW -> 0xBF 0b[1->read,0->not_mul][111111 -> and with reg addr]
	//R+MUL_RW -> 0xFF 0b[1->read, 1->mul][111111-> ..]
	//W+NOT_MUL_RW -> 0x3F 0b[0->write, 0->not_mul][111111->..]
	//W+MUL_RW -> 0x7F 0b[0->write, 1->mul][111111->..]
void LSM9DSO_write_reg(bool type, uint8_t reg, uint8_t value, uint8_t times);
uint8_t LSM9DSO_read_reg(bool type, uint8_t reg, uint8_t times);

void update_LSM9DSO_acc(uint8_t check);
void update_LSM9DSO_mag(uint8_t check);
void update_LSM9DSO_gyro(uint8_t check);

void enable_fifo_and_HPF_g();
void enable_fifo_xm();

void set_FIFO_mode_gyro(uint8_t mode);
void set_FIFO_mode_accmag(uint8_t mode);

void LSM9DS0_set_gyro_scale(uint8_t scale);
void LSM9DS0_set_acc_scale(uint8_t scale);
void LSM9DS0_set_mag_scale(uint8_t scale);

void calib_gyro_data();
void calib_acc_data();
void calib_mag_data();

uint8_t i_twos_complement(uint8_t number);
float linear_transformation(float base_1, float base_2, float base_1_new);
//float calc_new_sensitivity_gyr(float base, float new_value, float initial_sensitivity);

void LSM9DS0_enable_temperature();
void LSM9DSO_update_temp();

float calc_new_sens(float base, float diff, float change_senes);
void LSM9DS0_update_gyr_sensitivity();
void LSM9DS0_update_acc_sensitivity();
void LSM9DS0_update_mag_sensitivity();

#endif /* INC_LSM9DSO_H_ */
