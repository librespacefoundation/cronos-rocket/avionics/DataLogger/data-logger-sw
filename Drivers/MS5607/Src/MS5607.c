/*
   MS5607-02 SPI library for ARM STM32F103xx Microcontrollers - Main source file
   DATASHEET of MS5607 : https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=MS5607-02BA03&DocType=Data+Sheet&DocLang=English
*/


#include <MS5607.h>

/* Private SPI Handler */
static SPI_HandleTypeDef *hspi;

/* Private GPIO CS Pin Variables */
static GPIO_TypeDef *CS_GPIO_Port[2];
static uint16_t CS_Pin[2];

/* SPI Transmission Data */
static uint8_t SPITransmitData;

/* Private OSR Instantiations */
static uint8_t Pressure_OSR =  OSR_256;
static uint8_t Temperature_OSR =  OSR_256;

/* Private data holders */

/* PROM data structure */
static struct promData promData[2];
/* Unconpensated values structure */
static struct MS5607UncompensatedValues uncompValues[2];
/* Compensated values structure */
static struct MS5607Readings readings[2];

/** Reset and prepare for general usage.
 * This will reset the device and perform the PROM reading to find the conversion values and if
 * the communication is working.
 */
MS5607StateTypeDef MS5607_Init(SPI_HandleTypeDef *hspix, GPIO_TypeDef *GPIOx1, uint16_t GPIO_Pin1, GPIO_TypeDef *GPIOx2, uint16_t GPIO_Pin2) {
  hspi = hspix;
  CS_GPIO_Port[0] = GPIOx1;
  CS_GPIO_Port[1] = GPIOx2;

  CS_Pin[0] = GPIO_Pin1;
  CS_Pin[1] = GPIO_Pin2;

  for (int i = 0; i < 2; ++i) {
	  enableCSB(i);
	  SPITransmitData = RESET_COMMAND;
	  HAL_SPI_Transmit(hspi, &SPITransmitData, 1, 10);
	  HAL_Delay(3);

	  disableCSB(i);

	  MS5607PromRead(&promData[i], i);

	  if (promData[i].reserved == 0x00 || promData[i].reserved == 0xff)
	    return MS5607_STATE_FAILED;
  }

  return MS5607_STATE_READY;
}

/* Performs a reading on the devices PROM. */
void MS5607PromRead(struct promData *prom, int index){
  uint8_t   address;
  uint16_t  *structPointer;

  /* As the PROM is made of 8 16bit addresses I used a pointer for acessing the data structure */
  structPointer = (uint16_t *) prom;

  for (address = 0; address < 8; address++) {
    SPITransmitData = PROM_READ(address);
    enableCSB(index);
    HAL_SPI_Transmit(hspi, &SPITransmitData, 1, 10);
    /* Receive two bytes at once and stores it directly at the structure */
    HAL_SPI_Receive(hspi, structPointer, 2, 10);
    disableCSB(index);
    structPointer++;
  }

  /* Byte swap on 16bit integers*/
  structPointer = (uint16_t *) prom;
  for (address = 0; address < 8; address++) {
    uint8_t   *toSwap = (uint8_t *) structPointer;
    uint8_t secondByte = toSwap[0];
    toSwap[0] = toSwap[1];
    toSwap[1] = secondByte;
    structPointer++;
  }
}

/* Performs a reading on the devices PROM. */
void MS5607UncompensatedRead(){
  /*Sensor reply data buffer*/
  uint8_t reply[3];

  for (int index =0; index < 2; index++) {
	  enableCSB(index);

	  /* Assemble the conversion command based on previously set OSR */
	  SPITransmitData = CONVERT_D1_COMMAND | Pressure_OSR;
	  HAL_SPI_Transmit(hspi, &SPITransmitData, 1, 10);

	  disableCSB(index);
  }

  // Delay for both sensors
  if(Pressure_OSR == 0x00)
	HAL_Delay(1);
  else if(Pressure_OSR == 0x02)
	HAL_Delay(2);
  else if(Pressure_OSR == 0x04)
	HAL_Delay(3);
  else if(Pressure_OSR == 0x06)
	HAL_Delay(5);
  else
	HAL_Delay(10);

  for (int index =0; index < 2; index++) {
	  enableCSB(index);

	  /* Performs the reading of the 24 bits from the ADC */
	  SPITransmitData = READ_ADC_COMMAND;
	  HAL_SPI_Transmit(hspi, &SPITransmitData, 1, 10);
	  HAL_SPI_Receive(hspi, reply, 3, 10);

	  disableCSB(index);

	  /* Tranfer the 24bits read into a 32bit int */
	  uncompValues[index].pressure = ((uint32_t) reply[0] << 16) | ((uint32_t) reply[1] << 8) | (uint32_t) reply[2];
  }
  for (int index = 0; index < 2; ++index) {
	  enableCSB(index);

	  /* Assemble the conversion command based on previously set OSR */
	  SPITransmitData = CONVERT_D2_COMMAND | Temperature_OSR;
	  HAL_SPI_Transmit(hspi, &SPITransmitData, 1, 10);

	  disableCSB(index);
  }

  if(Temperature_OSR == 0x00)
	HAL_Delay(1);
  else if(Temperature_OSR == 0x02)
	HAL_Delay(2);
  else if(Temperature_OSR == 0x04)
	HAL_Delay(3);
  else if(Temperature_OSR == 0x06)
	HAL_Delay(5);
  else
	HAL_Delay(10);

  for(int index = 0; index < 2; ++index) {
	  enableCSB(index);

	  SPITransmitData = READ_ADC_COMMAND;
	  HAL_SPI_Transmit(hspi, &SPITransmitData, 1, 10);
	  HAL_SPI_Receive(hspi, reply, 3, 10);

	  disableCSB(index);

	  /* Assemble the conversion command based on previously set OSR */
	  uncompValues[index].temperature = ((uint32_t) reply[0] << 16) | ((uint32_t) reply[1] << 8) | (uint32_t) reply[2];
  }
}

/* Performs the data conversion according to the MS5607 datasheet */
void MS5607Convert(struct MS5607UncompensatedValues *sample, struct MS5607Readings *value, int index){
  int32_t dT;
  int32_t TEMP;
  int64_t OFF;
  int64_t SENS;

  dT = sample->temperature - ((int32_t) (promData[index].tref << 8));

  TEMP = 2000 + (((int64_t) dT * promData[index].tempsens) >> 23);

  OFF = ((int64_t) promData[index].off << 17) + (((int64_t) promData[index].tco * dT) >> 6);
  SENS = ((int64_t) promData[index].sens << 16) + (((int64_t) promData[index].tcs * dT) >> 7);

  /**/
  if (TEMP < 2000) {
    int32_t T2 = ((int64_t) dT * (int64_t) dT) >> 31;
    int32_t TEMPM = TEMP - 2000;
    int64_t OFF2 = (61 * (int64_t) TEMPM * (int64_t) TEMPM) >> 4;
    int64_t SENS2 = 2 * (int64_t) TEMPM * (int64_t) TEMPM;
    if (TEMP < -1500) {
      int32_t TEMPP = TEMP + 1500;
      int32_t TEMPP2 = TEMPP * TEMPP;
      OFF2 = OFF2 + (int64_t) 15 * TEMPP2;
      SENS2 = SENS2 + (int64_t) 8 * TEMPP2;
    }
    TEMP -= T2;
    OFF -= OFF2;
    SENS -= SENS2;
  }

  value->pressure = ((((int64_t) sample->pressure * SENS) >> 21) - OFF) >> 15;
  value->temperature = TEMP;
}

/* Performs the sensor reading updating the data structures */
void MS5607Update(void){
	MS5607UncompensatedRead();
	for (int i = 0; i < 2; ++i) {
		  MS5607Convert(&uncompValues[i], &readings[i], i);
	}
}

/* Gets the temperature from the sensor reading */
double MS5607GetTemperatureC(int index){
  return ((double)readings[index].temperature/(double)100.0 - 2);
}

/* Gets the pressure from the sensor reading */
int32_t MS5607GetPressurePa(int index){
  return (readings[index].pressure/(double)100.0 + 4); // in hPa
}

/* Sets the CS pin */
void enableCSB(int index){
  HAL_GPIO_WritePin(CS_GPIO_Port[index], CS_Pin[index], GPIO_PIN_RESET);
}

/* Sets the CS pin */
void disableCSB(int index){
  HAL_GPIO_WritePin(CS_GPIO_Port[index], CS_Pin[index], GPIO_PIN_SET);
}

/* Sets the OSR for temperature */
void MS5607SetTemperatureOSR(MS5607OSRFactors tOSR){
  Temperature_OSR = tOSR;
}

/* Sets the OSR for pressure */
void MS5607SetPressureOSR(MS5607OSRFactors pOSR){
  Pressure_OSR = pOSR;
}

/* ============================================================================================
 * The code which we adjusted into our project had the following notice:
 *
MS5607-02 device SPI library code for ARM STM32F103xx is placed under the MIT license
Copyright (c) 2020 João Pedro Vilas Boas
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
  *
 ================================================================================================
 */


