/*
 * bmp384.h
 *
 *  Created on: May 28, 2022
 *  Edited: July 20, 2022 by Vic
 *      Author: thodoris
 */

#ifndef BMP384_INC_BMP384_H_
#define BMP384_INC_BMP384_H_

#include "bmp384_registers.h"
#include "stm32l4xx_hal.h"
#include <stdint.h>
#include <stdbool.h>


typedef enum {
	SLEEP,
	FORCED,
	NORMAL
} bmp384_modes;

typedef struct {
	HAL_StatusTypeDef (* read)(uint16_t address, uint16_t MemAddSize, uint8_t *pData, uint16_t size) ;
	HAL_StatusTypeDef (* write)(uint16_t address, uint16_t MemAddSize, uint8_t *pData, uint16_t size);
	void (* delay)(uint32_t delay_ms);
} bmp384_driver;

typedef struct {
	uint32_t raw_pressure;
	uint32_t raw_temperature;

	double pressure;
	double temperature;
}bmp384_data;

typedef struct {
	uint16_t 			fifoWaterMark;

	uint8_t 			config1;
	uint8_t 			config2;

	uint8_t 			int_control;

	uint8_t				if_conf;

	// pwr_control
	bmp384_modes 		mode;
	uint8_t 			tempratureEn;
	uint8_t 			pressureEn;

	uint8_t 			osr_p;
	uint8_t 			osr_t;

	uint8_t 			odr_select;

	uint8_t 			configRegValue;

	uint16_t 			nop;
	uint16_t 			softreset;
} bmp384_settings;

typedef struct {
	uint8_t chip_id;

	// result from error register
	uint8_t fatal_err;
	uint8_t cmd_err;
	uint8_t conf_err;

	// result from status register
	uint8_t cmd_rdy; 	// CMD decoder status -> 0: Command in progress, 1: Command decoder is ready to accept a new command
	uint8_t drdy_press; // Data ready for pressure. It gets reset, when one pressure DATA register is read out
	uint8_t drdy_temp;  // Data ready for temperature sensor. It gets reset, when one temperature DATA register is read out

	// Data Registers
	uint32_t pressureData;
	uint32_t tempratureData;
	uint32_t sensorTimeData;

	// Event Sensor
	uint8_t por_detected; // ‘1’ after device power up or softreset. Clear-on-read

	// INT_STATUS
	uint8_t fwm_int; // FIFO Watermark Interrupt
	uint8_t ffull_int; // FIFO Full Interrupt
	uint8_t drdy; // data ready interrupt

} bmp384_sensor;

HAL_StatusTypeDef setFifoWtm();
HAL_StatusTypeDef setFifoConfRegisters();
HAL_StatusTypeDef setIntCtrlRegister();
HAL_StatusTypeDef setIfConfRegister();
HAL_StatusTypeDef setPowerCtrlRegister();
HAL_StatusTypeDef setOSRRegister();
HAL_StatusTypeDef setODRRegister();
HAL_StatusTypeDef setConfigRegister();
HAL_StatusTypeDef setCmd(uint8_t);

void CalibCoeffs2Float();
void TrimRead();
void setSettings();
void BMP384_Measure ();

uint8_t getChipId();
uint8_t getStatusFlags();
uint8_t getErrorStatus();
uint8_t getEventRegister();
uint8_t getIntStatusRegister();
HAL_StatusTypeDef initializeBmp384();

void BMPReadRawData(void);
void BMP384_compensate_temperature();
void BMP384_compensate_pressure();



#endif /* BMP384_INC_BMP384_H_ */
